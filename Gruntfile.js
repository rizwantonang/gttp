module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    uglify: {
      my_target:{
      	files: {
      		'js/scripts.js': ['javascript/*.js']
      	}
      }
    },// uglify

    jshint: {
    	options: {
    		force: true
    	},
    	all: ['Gruntfile.js', 'javascript/*.js'],
    }, // jshint

    autoprefixer:{
    	no_dest:{
    		src: 'css/app.css' // globbing is also posibble here
    	}
    }, // autoprefixer

    htmlhint: {
    	build:{
    		options:{
	            'tag-pair': true,
	            'tagname-lowercase': true,
	            'attr-lowercase': true,
	            'attr-value-double-quotes': true,
	            'doctype-first': true,
	            'spec-char-escape': true,
	            'id-unique': true,
	            'head-script-disabled': true,
	            'style-disabled': true
    		}, // options
    		src: ['*.html']
    	}
    }, // htmlhint

    sass: {
      dist: {
        files: {
          'css/app.css': 'sass/application.scss'
        }
      }
    }, // sass

    watch: {
      options: {
        livereload: true,
      },

    	gruntfile: {
    		files: 'Gruntfile.js',
    		tasks: ['jshint:gruntfile'],
    	}, // gruntfile

    	scripts: {
    		files: ['javascript/*.js'],
    		tasks: ['jshint', 'uglify'],
    	}, // scripts

    	css: {
    		files: '**/*.scss',
    		tasks: ['sass', 'autoprefixer'],
    	}, // css

      html: {
        files: ['*.html'],
        tasks: ['htmlhint:build'],
      }, // html

    } // watch

  });

  // Load the plugin[s] that provides task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-htmlhint');
  grunt.loadNpmTasks('grunt-contrib-watch');


  // Default task(s).
  grunt.registerTask( 'default', [ 'uglify', 'jshint', 'sass', 'autoprefixer', 'htmlhint', 'watch'] );

  // Server task(s)
  grunt.registerTask( 'serve', [ 'watch'] );

};