(function ($) {

	'use strict';
	// nav-top
	$('.nav-top ul').superfish({
		delay: 400,
		animation: {
			opacity: 'show',
			height: 'show'
		},
		animationOut: {
			opacity: 'hide',
			height: 'hide'
		},
		speed: 200,
		speedOut: 200,
		autoArrows: false
	});

	// main-nav
	$('.main-nav ul').superfish({
		delay: 400,
		animation: {
			opacity: 'show',
			height: 'show'
		},
		animationOut: {
			opacity: 'hide',
			height: 'hide'
		},
		speed: 200,
		speedOut: 200,
		autoArrows: false
	});

	// homepage-slider
	$('.homepage-slider').owlCarousel({
	    animateOut: 'fadeOut',
	    animateIn: 'fadeIn',
	    items:1,
	    margin:0,
	    stagePadding:0,
	    smartSpeed:450,
	    loop: true,
	    autoplay: true,
	    autoplayTimeout: 7000,
	    autoplayHoverPause: false,
	    // rtl: true,
	    nav: true,
	    navText: [  "<i class='fa fa-angle-left'></i>",
      "<i class='fa fa-angle-right'></i>" ]
	});

	// partner-carousel
	$('.partners-carousel').owlCarousel({
		items: 5,
		loop: true,
	    nav: true,
	    navText: [  "<i class='fa fa-angle-left'></i>",
      "<i class='fa fa-angle-right'></i>" ],
      	responsiveClass: true,
      	responsive: {
      		0: {
      			items: 2,
      			nav: true
      		},
      		600: {
      			items: 4,
      			nav: false
      		},
      		700: {
      			nav: true
      		}
      	}
	});

	// testimonial-slider
	$('.testimonial-slider').owlCarousel({
		animationOut: 'fadeOut',
		animationIn: 'fadeIn',
		items: 1,
		margin: 0,
		stagePadding: 0,
		smartSpeed: 450,
		loop: true,
		autoplay: true,
		autoplayTimeout: 7000,
		autoplayHoverPause: false,
	});

	// config Isotope
	var $container = $('.galleries-body');
	$container.imagesLoaded( function() {
		$container.isotope();
	});

	// config Isotope
	$('.community-filter a').click( function() {
		var selector = $(this).attr('data-filter');
		$container.isotope({
			itemSelector: '.gallery-item',
			filter: selector
		});
		return false;
	});

	// config Isotope
	$('.community-filter a').click( function (e) {
		$('.community-filter a').removeClass("active");
		$(this).addClass("active");
	});

	// video container
	$('.video-container').fitVids();

	// responsive-main-menu
	$('.main-nav > ul').clone(false).find("ul,li").removeAttr("id").remove(".submenu").appendTo($(".mobile-nav"));
	
	$('.nav-btn').on("click", function() {
		$('.mobile-nav').collapse({
			toggle: false
		});
	});

		 //DROPDOWN
    $("#ddNav").click(function () {
        $(".ddown-list ul").toggle();
    });
    $(".ddown-list ul li a").click(function () {
        var text = $(this).html();
        $("#ddNav span").html(text);
        $(".ddropdown ul").hide();
        //var source = $("#source");
        //source.val($(this).find("span.value").html())
    });
})(jQuery); 